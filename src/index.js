var lib = require('./lib'),
	Interval = require('ISO8601.js').Interval;

/**
 * @param {Object} client	Redis client
 * @param {String} scheme	util.format's format string
 * @param {String|Interval} interval
 * @param {Date} epoch
 * @param {Number|String|Interval} TTL
 */
function construct(client, scheme, interval, epoch, TTL) {
	scheme = scheme || 'ztime:%s';
	interval = interval instanceof Interval ? interval : new Interval(interval);
	epoch = epoch instanceof Date ? epoch : new Date(epoch);
	TTL = TTL || 0;

	if (typeof TTL === 'string')
		TTL = new Interval(TTL);
	if (TTL instanceof Interval)
		TTL = +TTL;

	var periodSharder = lib.intervalSinceEpoch(+interval, epoch),
		keyMaker = lib.getKey(scheme);

	return {
		incr: lib.increment(client, periodSharder, keyMaker, TTL), // identity, amount, time
		get: lib.get(client, periodSharder, keyMaker, interval) // from, to, identity
	};
};

module.exports = construct;