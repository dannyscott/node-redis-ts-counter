var util = require('util'),
	R = require('ramda'),
	Bluebird = require('bluebird'),
	Period = require('ISO8601.js').Period;

var intervalSinceEpoch = R.curry(function(interval, epoch, ts) {
	return Math.floor(((ts-epoch)/duration)/1000);
});

var getKey = R.curry(function(scheme, period) {
	return util.format(scheme, period);
});

var getRange = function(client, periodSharder, keyMaker, interval, from, to) {
	var range = new Period(new Duration(from, to), interval),
		multi = client.multi();

	R.forEach(multi.exists, range);

	return Bluebird.promisify(multi.exec)
		.then(function(replies) {
			return range.filter(function(value, index) {
				return replies[index];
			});
		});
});

var execMulti = function(client, cmds) {
	return Bluebird.promisify(client.multi(cmds).exec);
/*
	return new Bluebird(function(resolve, reject) {
		client.multi(cmds).exec(function(err, replies) {
			if (err) {
				reject(err);
			} else {
				resolve(replies);
			}
		});
	});
*/
};

var zipValueScores = function(result) {
	var retVal = {};
	for (var i = 0; i < result.length; i = i + 2)
		retVal[result[i]] = result[i + 1];
	return retVal;
};

var get = R.curryN(4, function(client, periodSharder, keyMaker, interval, from, to, identity) {
	var cmds = [],
		tmp = 'zt:' + uuidgen.v4();

	if (identity)
		cmds.push(['ZADD', tmp, 0, identity]);

	return getRange(client, periodSharder, keyMaker, interval, from, to)
		.then(function(keys) {
			var compute = [],
				retrieve,
				cleanup = ['DEL', tmp];

			if (identity) {
				compute.push('ZINTERSTORE');
				retrieve = ['ZSCORE', tmp, identity];
			} else {
				compute.push('ZUNIONSTORE');
				retrieve = ['ZRANGE', tmp, 0, -1, 'WITHSCORES'];
			}

			compute = compute.concat(tmp, keys.length, keys, 'AGGREGATE SUM')

			return execMulti(client, cmds.concat(compute, retrieve, cleanup));
		}).then(function(replies) {
			if (identity) {
				return replies[2];
			} else {
				return zipValueScores(replies[1]);
			}
		});
});

var increment = R.curryN(4, function(client, periodSharder, keyMaker, TTL, identity, incrValue, time) {
	time = time || new Date();
	incrValue = incrValue || 1;

	var key = keyMaker(periodSharder(time)),
		params = [key, incrValue, identity];

	if (TTL)
		params = params.concat(['EX', TTL]);

	return new Bluebird(function(resolve, reject) {
		client.zincrby(params, function(err, response) {
			if (err) {
				resolve(err);
			} else {
				reject(Number(response));
			}
		});
	});
});

module.exports = {
	intervalSinceEpoch: intervalSinceEpoch,
	getKey: getKey,
	getRange: getRange,
	execMulti: execMulti,
	zipValueScores: zipValueScores,
	get: get,
	increment: increment
};