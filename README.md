```
#!javascript

var client, // redis client
	scheme = 'p:caps:%s', // key naming format
	interval = 'PT24H', // duration of each slice
	epoch = new Date('2014-01-01'), // epoch used when counting slices
	TTL = 'P1M'; // max lifetime of each slice in redis

var zcap = construct(client, scheme, interval, epoch, TTL);

zcap.incr('uuid'); // +1 in latest slice
zcap.incr('uuid', 20); // + 20 in latest slice
zcap.incr('uuid', 5, new Date("2014-01-05")); // + 5 in "2014-01-05" slice

zcap.get(new Date('2014-01-01'), new Date('2014-01-15'), 'uuid'); // === 26 (1 + 20 + 5);
```